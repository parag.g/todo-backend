const { Client } = require("pg");
require('dotenv').config();

global.client = new Client({
    'user': process.env.DB_USER,
    'host': process.env.DB_HOST,
    'database': process.env.DB_NAME,
    'port': process.env.DB_PORT,
    'password': process.env.DB_PASSWORD
});

client.connect((error) => {
    if (error) {
        console.log(error);
        return;
    }
    console.log("Connection to DB successful!");
});